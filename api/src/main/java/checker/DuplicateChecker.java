package checker;

import java.util.Collection;

/**
 * 3 task
 */
public interface DuplicateChecker {
    Collection<CountPerson> check(Collection<Person> persons);
}
