package checker;

import java.util.Collection;

/**
 * 1 task
 */
public interface RuleChecker {
    Collection<Person> check(Collection<Person> collection);
}
