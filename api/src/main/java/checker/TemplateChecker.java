package checker;

import java.util.Collection;

/**
 * 2 task
 */
public interface TemplateChecker {

    boolean check (Collection<Person> collection, Person person);

}
