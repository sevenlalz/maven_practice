package checker;

import lombok.*;

import java.util.Comparator;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CountPerson {
    private Person person;

    private int count;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(count);
        builder.append(":");
        builder.append("{");
        builder.append(person);
        builder.append("}");
        builder.append("}");
        return builder.toString();
    }

    public static class CompareWithAge implements Comparator<CountPerson> {
        @Override
        public int compare(CountPerson o1, CountPerson o2) {
            return Integer.compare(o2.getPerson().getAge(), o1.getPerson().getAge());
        }
    }
}
