package forex.configs;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ForexServlet extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfigurerClasses.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{LocalForexServletConfigurer.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/", "/valute"};
    }
}
