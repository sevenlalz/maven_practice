package forex.configs;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Пока необязателен
 */
@Configuration
@EnableWebMvc
public class LocalForexServletConfigurer implements WebMvcConfigurer {

}
