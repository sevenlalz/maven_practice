package forex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ValuteDto {

    @JsonProperty("Date")
    private String date;

    @JsonProperty("Valute")
    private Map<String, ValuteProperties> valutePropertiesList;

    @Data
    public static class ValuteProperties {

        @JsonProperty("ID")
        private String id;

        @JsonProperty("Name")
        private String name;

        @JsonProperty("Value")
        private double value;

        @JsonProperty("Previous")
        private double previous;
    }
}
