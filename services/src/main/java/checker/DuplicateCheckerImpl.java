package checker;

import java.util.*;

public class DuplicateCheckerImpl implements DuplicateChecker {
    @Override
    public Collection<CountPerson> check(Collection<Person> persons) {
        List<CountPerson> countPersonList = new LinkedList<>();
        int count;
        for (Person p : persons) {
            count = Collections.frequency(persons, p);
            CountPerson temp = new CountPerson();
            temp.setCount(count);
            temp.setPerson(p);
            countPersonList.add(temp);
        }

        Set<CountPerson> countPersonSet = new TreeSet<>(new CountPerson.CompareWithAge());
        countPersonSet.addAll(countPersonList);

        return countPersonSet;
    }
}
