package checker;

import java.util.Collection;
import java.util.LinkedList;

import java.util.stream.Collectors;

public class RuleCheckerImpl implements RuleChecker {

    public Collection<Person> check(Collection<Person> collection) {
        return collection.stream()
                .filter(human -> human.getAge() <= 20)
                .filter(human -> Character.toString(human.getName().charAt(0)).matches("[a-dA-D]"))
                .collect(Collectors.toCollection(LinkedList::new));
    }
}

//git remote позволяет "залогиниться" на git