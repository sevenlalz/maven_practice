package checker;

import java.util.Collection;

public class TemplateCheckerImpl implements TemplateChecker{

    @Override
    public boolean check(Collection<Person> collection, Person person) {
        return collection.contains(person);
    }
}
