package forex.service;

import forex.ValuteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ValuteController {

    private final ValuteService valuteService;

    @Autowired
    public ValuteController(@Qualifier("valuteService") ValuteService valuteService) {
        this.valuteService = valuteService;
    }

    @GetMapping("/")
    public ValuteDto getValute() {
        System.out.println("asd");
        return valuteService.checkValute();
    }
}
