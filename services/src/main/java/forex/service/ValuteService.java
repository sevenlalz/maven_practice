package forex.service;

import forex.ValuteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class ValuteService {

    @Value("https://www.cbr-xml-daily.ru/daily_json.js")
    private String URL;

    private final RestTemplate template;

    @Autowired
    public ValuteService(@Qualifier("template") RestTemplate template) {
        this.template = template;
    }


    public ValuteDto checkValute() {
       return template.getForObject(URL, ValuteDto.class);
    }
}
