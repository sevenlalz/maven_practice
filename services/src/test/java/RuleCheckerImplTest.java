
import checker.Person;
import checker.RuleCheckerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

class RuleCheckerImplTest {

    @Test
    void check() {
        List<Person> actual = new LinkedList<>();
        var p1 = new Person("Alex", "Tsi", "Abdurasulovich", 23, "Male", new Date(1997, Calendar.AUGUST, 3));
        var p2 = new Person("Alexey", "Smirnov", "Valerevich", 15, "Male", new Date(2006, Calendar.JUNE, 30));
        var p3 = new Person("Maria", "Ponamoreva", "Alekseevna", 43, "FeMale", new Date(1977, Calendar.JULY, 15));
        var p4 = new Person("Vyacheslav", "Petrosov", "Evgenevich", 33, "Male", new Date(1987, Calendar.APRIL, 27));
        var p5 = new Person("Alex", "Tsi", "Abdurasulovich", 23, "Male", new Date(1997, Calendar.AUGUST, 3));
        actual.add(p1);
        actual.add(p2);
        actual.add(p3);
        actual.add(p4);
        actual.add(p5);

        var ruleChecker = new RuleCheckerImpl();

        Assertions.assertEquals("[" + p2 + "]", ruleChecker.check(actual).toString());
    }
}