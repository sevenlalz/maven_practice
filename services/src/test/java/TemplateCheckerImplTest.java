import checker.Person;
import checker.TemplateCheckerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

class TemplateCheckerImplTest {

    @Test
    void check() {

        List<Person> people = new LinkedList<>();
        people.add(new Person("Alex", "Tsi", "Abdurasulovich", 23, "Male", new Date(1997, Calendar.AUGUST, 3)));
        people.add(new Person("Alexey", "Smirnov", "Valerevich", 15, "Male", new Date(2006, Calendar.JUNE, 30)));
        people.add(new Person("Maria", "Ponamoreva", "Alekseevna", 43, "FeMale", new Date(1977, Calendar.JULY, 15)));
        people.add(new Person("Vyacheslav", "Petrosov", "Evgenevich", 33, "Male", new Date(1987, Calendar.APRIL, 27)));
        people.add(new Person("Alex", "Tsi", "Abdurasulovich", 23, "Male", new Date(1997, Calendar.AUGUST, 3)));

        Person checkFor = new Person("Alex", "Tsi", "Abdurasulovich", 23, "Male", new Date(1997, Calendar.AUGUST, 3));

        var actual = new TemplateCheckerImpl();


        Assertions.assertTrue(actual.check(people, checkFor));
    }
}